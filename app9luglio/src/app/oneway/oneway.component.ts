import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-oneway',
  templateUrl: './oneway.component.html',
  styleUrls: ['./oneway.component.scss']
})
export class OnewayComponent implements OnInit {

  name= "Nino";
  stamp="";
  isDisabled=true;  


  constructor() { }

  ngOnInit() {
  }
  onClick(event){
    console.log(event);
    this.stamp="Cosi stampo il valore della variabile stamp alla quale era stata assegnata una stringa vuota.";
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChiamateComponent } from './chiamate/chiamate.component';
import { DirettiveComponent } from './direttive/direttive.component';
import { FormComponent } from './form/form.component';
import { BindComponent } from './bind/bind.component';
import { BodyComponent } from './body/body.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { OnewayComponent } from './oneway/oneway.component';
import { TwowayComponent } from './twoway/twoway.component';


const routes: Routes = [
  {path: '', component: BodyComponent, pathMatch: 'full'},
  {path: "header", component: HeaderComponent},
  {path: "footer", component: FooterComponent},
  {path: "direttive", component: DirettiveComponent},
  {path: "form", component: FormComponent},
  {path: "bind", component: BindComponent},
  {path: "chiamate", component: ChiamateComponent},
  {path: "oneway", component: OnewayComponent},
  {path: "twoway", component: TwowayComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

